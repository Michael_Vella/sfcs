﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiEstimation
{
    class Program
    {
        const int n = 100000000;

        static void Main(string[] args)
        {
            Random r = new Random();

            int count = 0;

            // for every required point
            for (int i = 0; i < n; i++)
            {
                // generate a point on unit square
                double x = r.NextDouble();
                double y = r.NextDouble();

                // if the point falls on circle
                if (Math.Pow(x,2) + Math.Pow(y,2) < 1)
                {
                    count++;
                }
            }

            double piEstimate = (4.0 * count) / n;

            Console.WriteLine(
                String.Format("Using n={0} points, the estimate for Pi is {1}", n, piEstimate));
            Console.ReadKey();

        }
    }
}
