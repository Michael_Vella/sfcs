﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Distributions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 20000;
            double[] twodDMeanEstimate = TwoDMean(n);
            double[] threeDMeanEstimate = ThreeDMean(n);
            double[] twoDSD = Confidence99((SDev(n,twodDMeanEstimate)),
                                           (twodDMeanEstimate.Average()),n);
            double[] threeDSD = Confidence99((SDev(n, threeDMeanEstimate)),
                                            (threeDMeanEstimate.Average()), n); 

            Console.WriteLine("2D mean length of L\n" +
                              "Mean length of l after {0} runs is: {1}",n,
                             twodDMeanEstimate.Average());
            Console.WriteLine("Lower and Upper bounds of Stand Dev (99% confidence)\n" +
                             "lower: {0}\tupper: {1}\n", twoDSD[0],
                             twoDSD[1]);
            Console.WriteLine("============================================\n");
            Console.WriteLine("3D mean length of L\n" +
                              "Mean length of l after {0} runs is: {1}", n,
                             threeDMeanEstimate.Average());
            Console.WriteLine("Lower and Upper bounds of Stand Dev (99% confidence)\n" +
                           "lower: {0}\tupper: {1}\n", threeDSD[0],
                           threeDSD[1]);

            
            Console.WriteLine("============================================\n");
            double[] variance = ThreeDVariation(n);
            double varianceSd = SDev(n, variance);
            double[] varianceConf99 = Confidence99(varianceSd, variance.Average(), n);

            Console.WriteLine("Sample Mean of the Variance is {0}", variance.Average());
            Console.WriteLine("Estimation of SDV: {0}", varianceSd);
            Console.WriteLine("Lower Bound {0}\tupper {1}", varianceConf99[0], varianceConf99[1]);
            Console.ReadLine();
        }

        /// <summary>
        /// Returns the estimate mean length of a line  
        /// </summary>
        public static double[] TwoDMean(int n) {

            Random rand = new Random();

            double[] l = new double[n];
            double x_1, x_2, y_1, y_2;

            for (int i = 0; i < n; i++)
            {
                x_1 = rand.NextDouble();
                x_2 = rand.NextDouble();
                y_1 = rand.NextDouble();
                y_2 = rand.NextDouble();

                l[i] = Math.Sqrt((Math.Pow((x_2 - x_1), 2) +
                              Math.Pow(y_2 - y_1, 2))
                             );

                //l =  Math.Sqrt((Math.Pow((y_1 - x_1), 2) +
                //               Math.Pow(y_2 - x_2, 2))
                //              );
            }

            return l;
        }


        /// <summary>
        /// Returns the estimate mean length of a line  
        /// </summary>
        public static double[] ThreeDMean(int n)
        {

            Random rand = new Random();

            double[] l =new double[n];
            double x_1, x_2, y_1, y_2,z_1,z_2;

            for (int i = 0; i < n; i++)
            {
                
                x_1 = rand.NextDouble();
                x_2 = rand.NextDouble();
                y_1 = rand.NextDouble();
                y_2 = rand.NextDouble();
                z_1 = rand.NextDouble();
                z_2 = rand.NextDouble();

                l[i] = Math.Sqrt((Math.Pow((x_2 - x_1), 2) +
                               Math.Pow(y_2 - y_1, 2) + 
                               Math.Pow(z_2-z_1,2))
                              );
            }

            return l;
        }


        /// <summary>
        /// Calculate the standard deviation.
        /// </summary>
        /// <param name="sampleSize"></param>
        /// <param name="y_i"></param>
        /// <param name="sampleMean"></param>
        /// <returns>Returns an array that has the lower bount first and then the upper bound second</returns>
        public static double SDev(int sampleSize, double[] y_i) {
            //double[] s2 = new double[sampleSize];
            //double yMew = 0;

            ////Calculate Standard Dev
            //for (int i = 0; i < sampleSize; i++)
            //{
            //    yMew = y_i[i] - sampelMean;
            //    s2[i] = (1 / sampleSize - 1) * yMew;
            //}


            //Code based on answer : https://stackoverflow.com/questions/5336457/how-to-calculate-a-standard-deviation-array
            double average = y_i.Average();
            double sumOfSquaresOfDiff = y_i.Select(x => (x - average) * (x - average)).Sum();
            double sd = Math.Sqrt(sumOfSquaresOfDiff / (sampleSize-1));

            return sd;
            //Get lower and Upper bounds.

            
        }

        public static double[] Confidence99(double sd, double average, int sampleSize) {
            double[] lowerUpper = new double[2];
            lowerUpper[0] = average - (2.58 * (sd / Math.Sqrt(sampleSize)));
            lowerUpper[1] = average + (2.58 * (sd / Math.Sqrt(sampleSize)));

            return lowerUpper;
        }
        
        /// <summary>
        /// Returns the variance from the ThreeDMean just pass the n as the amount of calculations.  
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static double[] ThreeDVariation(int n) {
            //based on: https://stackoverflow.com/questions/218060/random-gaussian-variables#comment83380601_15556411

            Random rand = new Random();
            double[] varianceArray = new double[n];

            for (int i = 0; i < n; i++)
            {
                varianceArray[i] = Variance(0, 1, rand);
            }

            return varianceArray;
        }

        public static double Variance(double mean, double sdv, Random random) {

             Normal normal = new Normal(mean, sdv, random);
             return normal.Sample();
        }

    }
}