﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GaltonBoardSimulation
{
    public class GaltonBoard
    {
        private static Random r = new Random();

        public int PinLayers { get; private set; } = 8;
        public double ProbabilityRight { get; private set; } = 0.5;

        public GaltonBoard(int pinLayers = 8, double probabilityRight = 0.5)
        {
            PinLayers = pinLayers;
            ProbabilityRight = probabilityRight;
        }

        public int DropBean()
        {
            int count = 0;

            for (int i = 0; i < PinLayers; i++)
            {
                double x = r.NextDouble();

                if (x < ProbabilityRight)
                    count++;    
            }

            return count;
        }

        public int[] DropBeans(int n)
        {
            int[] output = new int[PinLayers + 1];

            for (int i = 0; i < n; i++)
            {
                int beanResult = DropBean();
                output[beanResult]++;
            }

            return output;
        }
            
    }
}
