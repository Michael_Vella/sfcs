﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GaltonBoardSimulation
{
    class Program
    {
        static void Main(string[] args)
        {
            GaltonBoard board = new GaltonBoard();
            int[] result = board.DropBeans(1000);

            PrintResults(result);

            Console.ReadKey();
        }

        private static void PrintResults(int[] result)
        {
            for (int i = 0; i < result.Length; i++)
            {
                Console.WriteLine(i + ", " + result[i]);
            }
        }
    }
}
